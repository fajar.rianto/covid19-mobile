package com.diagnosiscovid.features.signin

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.diagnosiscovid.data.repository.signin.SignInRepository
import com.diagnosiscovid.data.repository.signin.model.SignInResponse
import com.diagnosiscovid.data.repository.user.UserRepository
import com.diagnosiscovid.network.NetworkBuilder
import com.diagnosiscovid.utils.PrefsUtils
import com.diagnosiscovid.utils.isValidEmail
import kotlinx.coroutines.launch

class SignInViewModel : ViewModel() {

  private val apiService = NetworkBuilder.getService()
    private val repository = SignInRepository(apiService)
    private val repositoryUser = UserRepository(apiService)

  val email = MutableLiveData<String>()
  val password = MutableLiveData<String>()

  val response = MutableLiveData<SignInResponse>(null)

  fun isValidToSignIn(): Boolean {
    return email.value?.isValidEmail() == true &&
          password.value?.isNotEmpty() == true &&
          password.value?.length ?: 0 >= 8
  }

  fun signIn() {
    viewModelScope.launch {
        val signInResponse = repository.signIn(
            email = email.value.toString(),
            password = password.value.toString()
        )
        val user = repositoryUser.getUsers(email.value.toString())
        PrefsUtils.saveUser(user)

        response.value = signInResponse
    }
  }

}