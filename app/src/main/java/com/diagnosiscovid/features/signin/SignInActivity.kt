package com.diagnosiscovid.features.signin

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.diagnosiscovid.R
import com.diagnosiscovid.databinding.ActivitySignInBinding
import com.diagnosiscovid.features.main.MainActivity
import com.diagnosiscovid.features.signup.SignUpActivity
import com.diagnosiscovid.utils.PrefsUtils
import com.diagnosiscovid.utils.withClickableSpan

class SignInActivity : AppCompatActivity() {
  private lateinit var binding: ActivitySignInBinding
  private lateinit var viewModel: SignInViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    if (checkUserAlreadySignIn()) return

    viewModel = ViewModelProvider(this).get(SignInViewModel::class.java)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)
    binding.viewModel = viewModel
    binding.lifecycleOwner = this

    setSpanLogin()
    observer()
    listener()
  }

  private fun checkUserAlreadySignIn(): Boolean {
    if (PrefsUtils.isUserSignedIn() == true) {
      showMainActivity()
      return true
    }
    return false
  }

  private fun listener() {
    binding.buttonSignIn.setOnClickListener {
      setDisabledAllFields()
      viewModel.signIn()
    }
  }

  private fun setDisabledAllFields() {
    binding.layoutInputEmail.isEnabled = false
    binding.layoutInputPassword.isEnabled = false
    binding.buttonSignIn.text = ""
    binding.progressBar.visibility = View.VISIBLE
  }

  private fun observer() {
    with(viewModel) {
      email.observe(this@SignInActivity) {
        setEnabledButtonSignIn()
      }
      password.observe(this@SignInActivity) {
        setEnabledButtonSignIn()
      }
      response.observe(this@SignInActivity) {
        if (it != null) {
          if (it.success?.token != null && it.error == null) {
            PrefsUtils.saveToken(it.success.token)
            showMainActivity()
          } else if (it.error != null) {
            setResetAllFields()
            Toast.makeText(
              this@SignInActivity,
              "Terjadi Kesalahan, silahkan login kembali.",
              Toast.LENGTH_SHORT
            ).show()
          }
        }
      }
    }
  }

  private fun setResetAllFields() {
    binding.layoutInputEmail.isEnabled = true
    binding.layoutInputPassword.isEnabled = true
    binding.buttonSignIn.text = "Sign In"
    binding.progressBar.visibility = View.GONE
  }

  private fun setEnabledButtonSignIn() {
    binding.buttonSignIn.isEnabled = viewModel.isValidToSignIn()
  }

  private fun setSpanLogin() {
    val span = SpannableString(binding.textSignIn.text).withClickableSpan(
      context = this,
      startChar = 25,
      endChar = 31
    ) { showRegisterActivity() }

    binding.textSignIn.movementMethod = LinkMovementMethod.getInstance()
    binding.textSignIn.setText(span, TextView.BufferType.SPANNABLE)
    binding.textSignIn.isClickable = true
  }

  private fun showRegisterActivity() {
    val intent = Intent(this, SignUpActivity::class.java)
    startActivity(intent)
    finish()
  }

  private fun showMainActivity() {
    val intent = Intent(this, MainActivity::class.java)
    startActivity(intent)
    finish()
  }
}