package com.diagnosiscovid.features.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.diagnosiscovid.R
import com.diagnosiscovid.databinding.ActivityMainBinding
import com.diagnosiscovid.features.main.home.HomeFragment
import com.diagnosiscovid.features.main.profile.ProfileFragment
import com.diagnosiscovid.features.main.questionnaire.QuestionnaireFragment

open class MainActivity : AppCompatActivity() {

  private val homeFragment: Fragment = HomeFragment()
    private val questionnaireFragment: Fragment = QuestionnaireFragment()
    private val profileFragment: Fragment = ProfileFragment()

  private var currentlyActiveFragment: Fragment = homeFragment

  open lateinit var binding: ActivityMainBinding

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

      binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

      supportFragmentManager.beginTransaction()
          .add(R.id.container_home, homeFragment, "HOME_FRAGMENT")
          .commit()
      supportFragmentManager.beginTransaction()
          .add(R.id.container_home, questionnaireFragment, "QUESTIONNAIRE_FRAGMENT")
          .hide(questionnaireFragment)
          .commit()
      supportFragmentManager.beginTransaction()
          .add(R.id.container_home, profileFragment, "PROFILE_FRAGMENT")
          .hide(profileFragment)
          .commit()

      listener()
  }

  private fun listener() {
    binding.bottomNavigation.setOnItemSelectedListener { menuItem ->
      when (menuItem.itemId) {
        R.id.home -> {
          supportFragmentManager.beginTransaction().hide(currentlyActiveFragment)
              .show(homeFragment).commit()
            currentlyActiveFragment = homeFragment
            return@setOnItemSelectedListener true
        }
          R.id.questionnaire -> {
              supportFragmentManager.beginTransaction().hide(currentlyActiveFragment)
                  .show(questionnaireFragment).commit()
              currentlyActiveFragment = questionnaireFragment
              return@setOnItemSelectedListener true
          }
          R.id.profile -> {
              supportFragmentManager.beginTransaction().hide(currentlyActiveFragment)
                  .show(profileFragment).commit()
              currentlyActiveFragment = profileFragment
              return@setOnItemSelectedListener true
          }
      }
      false
    }
  }
}