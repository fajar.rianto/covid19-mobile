package com.diagnosiscovid.features.main.home.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.diagnosiscovid.R
import com.diagnosiscovid.data.repository.home.model.HomeIntroModel
import com.diagnosiscovid.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {

  lateinit var binding: ActivityDetailBinding

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)

    val model = intent.getParcelableExtra<HomeIntroModel>("intent_key")

    Glide.with(this)
      .load(model?.detail?.imageHeader)
      .into(binding.imageHeader)

    Glide.with(this)
      .load(model?.detail?.imageDesc)
      .into(binding.imageDesc)
  }
}