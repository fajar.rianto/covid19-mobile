package com.diagnosiscovid.features.main.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.diagnosiscovid.data.repository.home.HomeRepository
import com.diagnosiscovid.data.repository.home.model.HomeIntroModel
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

  private val repository = HomeRepository()

  val response = MutableLiveData<List<HomeIntroModel>>()

  init {
    getIntroList()
  }

  private fun getIntroList() {
    viewModelScope.launch {
      response.value = repository.getIntroList()
    }
  }

}