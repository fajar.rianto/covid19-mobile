package com.diagnosiscovid.features.main.profile

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.diagnosiscovid.R
import com.diagnosiscovid.databinding.FragmentProfileBinding
import com.diagnosiscovid.features.signin.SignInActivity
import com.diagnosiscovid.utils.PrefsUtils
import com.github.drjacky.imagepicker.ImagePicker
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class ProfileFragment : Fragment() {

  private lateinit var viewModel: ProfileViewModel
  private lateinit var binding: FragmentProfileBinding

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {

    viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
    binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
    binding.viewModel = viewModel
    binding.lifecycleOwner = this

    return binding.root
  }

  var storage: FirebaseStorage? = null
  var storageReference: StorageReference? = null
  var fileName = ""

  private val launcher = registerForActivityResult(
    ActivityResultContracts.StartActivityForResult()
  ) {
    if (it.resultCode == Activity.RESULT_OK) {
      val uri = it.data?.data

      if (uri != null) {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setTitle("Updating profile photo...")
        progressDialog.show()

        val userPref = PrefsUtils.getUser()

        val ref: StorageReference? = storageReference?.child("${userPref.email}/${userPref.id}")
        ref?.putFile(uri)?.addOnSuccessListener {
          progressDialog.dismiss()

          Glide.with(requireContext())
            .load(uri)
            .into(binding.imageProfile)

        }?.addOnFailureListener {
          Toast.makeText(requireContext(), "Gagal mengubah photo", Toast.LENGTH_SHORT).show()

        }?.addOnProgressListener { taskSnapshot ->
          val progress: Double = (100.0
                * taskSnapshot.bytesTransferred
                / taskSnapshot.totalByteCount)
          progressDialog.setMessage("Uploaded " + progress.toInt() + "%")

        }
      }

    }
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    setEnabledField(false)

    storage = FirebaseStorage.getInstance()
    storageReference = storage?.reference

    setPhoto()

    setListener()
    setObserver()
  }

  private fun setPhoto() {
    val userPref = PrefsUtils.getUser()
    storageReference?.child("${userPref.email}/${userPref.id}")
      ?.downloadUrl
      ?.addOnSuccessListener {
        Glide.with(requireContext())
          .load(it)
          .into(binding.imageProfile)
      }
  }

  private fun setObserver() {
    viewModel.isSuccess.observe(viewLifecycleOwner) { success ->
      if (success != null) {
        if (success) {
          Toast.makeText(requireContext(), "Profile berhasil diperbarui", Toast.LENGTH_SHORT).show()
        }

        resetUpdateProfile()
      }
    }
  }

  private fun setListener() {
    setDropDownGenderList()

    binding.imageProfile.setOnClickListener {
      launcher.launch(
        activity?.let { parent ->
          ImagePicker.with(parent)
            .cropSquare()
            .galleryMimeTypes(
              mimeTypes = arrayOf(
                "image/png",
                "image/jpg",
                "image/jpeg"
              )
            )
            .setImageProviderInterceptor { imageProvider ->
              fileName = imageProvider.name
            }
            .galleryOnly()
            .createIntent()
        }
      )
    }

    binding.autoCompleteGenderProfile.setOnClickListener {
      setDropDownGenderList()
    }

    binding.buttonEdit.setOnClickListener {
      setEnabledField(true)
      binding.layoutInputName.isFocusable = true
      binding.buttonSave.visibility = View.VISIBLE
      binding.buttonCancel.visibility = View.VISIBLE
      binding.buttonEdit.visibility = View.GONE
      binding.buttonLogout.visibility = View.GONE
    }

    binding.buttonLogout.setOnClickListener {
      PrefsUtils.clearSession()
      startActivity(Intent(requireContext(), SignInActivity::class.java))
      activity?.finishAffinity()
    }

    binding.buttonCancel.setOnClickListener {
      viewModel.name.value = viewModel.getUser.value?.name
      viewModel.gender.value = viewModel.getUser.value?.gender
      viewModel.address.value = viewModel.getUser.value?.address
      viewModel.username.value = viewModel.getUser.value?.username

      resetUpdateProfile()
    }

    binding.buttonSave.setOnClickListener {
      setLoading()
      viewModel.saveUser()
    }
  }

  private fun setDropDownGenderList() {
    val items = listOf("Laki-Laki", "Perempuan")
    val adapter = ArrayAdapter(requireContext(), R.layout.gender_item, items)
    binding.autoCompleteGenderProfile.setAdapter(adapter)
    binding.autoCompleteGenderProfile.keyListener = null
  }

  private fun setLoading() {
    setEnabledField(false)
    binding.buttonSave.isEnabled = false
    binding.buttonCancel.isEnabled = false

    binding.progressBar.visibility = View.VISIBLE
  }

  private fun resetUpdateProfile() {
    binding.progressBar.visibility = View.GONE

    setEnabledField(false)

    binding.layoutInputName.clearFocus()
    binding.buttonSave.isEnabled = true
    binding.buttonCancel.isEnabled = true

    binding.buttonSave.visibility = View.GONE
    binding.buttonCancel.visibility = View.GONE
    binding.buttonEdit.visibility = View.VISIBLE
    binding.buttonLogout.visibility = View.VISIBLE
  }

  private fun setEnabledField(enabled: Boolean) {
    binding.layoutInputName.isEnabled = enabled
    binding.layoutInputGender.isEnabled = enabled
    binding.layoutInputAddress.isEnabled = enabled
    binding.layoutInputUsername.isEnabled = enabled
  }

}