package com.diagnosiscovid.features.main.questionnaire

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.diagnosiscovid.R
import com.diagnosiscovid.databinding.FragmentQuestionnaireBinding
import com.diagnosiscovid.utils.PrefsUtils
import kotlinx.coroutines.launch

class QuestionnaireFragment : Fragment() {

  private lateinit var viewModel: QuestionnaireViewModel
  private lateinit var binding: FragmentQuestionnaireBinding

  private lateinit var questionAdapter: QuestionnaireAdapter


  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {

    viewModel = ViewModelProvider(this).get(QuestionnaireViewModel::class.java)
    binding = DataBindingUtil.inflate(inflater, R.layout.fragment_questionnaire, container, false)

    return binding.root
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    initObserver()

    if (PrefsUtils.hasUserHasSubmitQuestionnaire()) {
      initSwipeRefresh()

      hideQuestionnaire()

      binding.progressBar.visibility = View.VISIBLE
      lifecycleScope.launch { viewModel.getDiagnoseResults() }
      return
    }

    binding.buttonSubmit.setOnClickListener {
      viewModel.submitQuestionnaire(questionAdapter.payload)
    }
  }

  private fun initObserver() {
    viewModel.questionsResponse.observe(viewLifecycleOwner) {
      questionAdapter = QuestionnaireAdapter(it)
      binding.recyclerQuestion.apply {
        layoutManager = LinearLayoutManager(requireContext())
        adapter = questionAdapter
      }
    }

    viewModel.isSuccessSubmitted.observe(viewLifecycleOwner) { isSuccess ->
      if (isSuccess) {
        hideQuestionnaire()
        showStateHarapTunggu()
        PrefsUtils.saveUserSubmittedQuestionnaire()
      }
    }

    viewModel.diagnoseResults.observe(viewLifecycleOwner) { hasilDiagnoses ->
      binding.progressBar.visibility = View.GONE
      hideQuestionnaire()

      if (binding.swipeContainer.isRefreshing) {
        binding.swipeContainer.isRefreshing = false
      }

      if (hasilDiagnoses.isNotEmpty()) {
        val result = hasilDiagnoses.filter { it.hasil != null }
        showHasilDiagnosis(result[0].hasil)
      } else if (hasilDiagnoses.isEmpty()) {
        showStateHarapTunggu()
      }
    }
  }

  private fun showHasilDiagnosis(hasilDiagnosis: String?) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      binding.stateHasilDiagnosis.hasilDiagnosis.text = Html.fromHtml(
        getString(
          R.string.hasil_diagnosis,
          hasilDiagnosis
        ),
        Html.FROM_HTML_MODE_COMPACT
      )
    } else {
      binding.stateHasilDiagnosis.hasilDiagnosis.text = Html.fromHtml(
        getString(
          R.string.hasil_diagnosis,
          hasilDiagnosis
        )
      )
    }
    binding.textHeader.text = "Hasil Diagnosis"
    binding.stateHasilDiagnosis.parentHasilDiagnosis.visibility = View.VISIBLE
    hideQuestionnaire()
  }

  private fun showStateHarapTunggu() {
    binding.textHeader.text = "Hasil Diagnosis"
    binding.stateHarapTunggu.containerHarapTunggu.visibility = View.VISIBLE
    hideQuestionnaire()
  }

  private fun hideQuestionnaire() {
    binding.recyclerQuestion.visibility = View.GONE
    binding.buttonSubmit.visibility = View.GONE
  }

  private fun initSwipeRefresh() {
    binding.swipeContainer.setOnRefreshListener {
      lifecycleScope.launch { viewModel.getDiagnoseResults() }
    }
  }
}