package com.diagnosiscovid.features.main.home

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
<<<<<<< HEAD
import androidx.core.content.ContextCompat.startActivities
=======
import android.widget.TextView
>>>>>>> ca930b258180c4aa605bb11421216122730cb052
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.setContentView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.diagnosiscovid.R
import com.diagnosiscovid.databinding.FragmentHomeBinding
<<<<<<< HEAD
import com.diagnosiscovid.features.main.Explaining
import com.diagnosiscovid.features.main.MainActivity
import com.diagnosiscovid.features.main.Prevention
=======
import com.diagnosiscovid.features.main.home.detail.DetailActivity
import com.diagnosiscovid.utils.withClickableSpan
import com.google.android.material.bottomnavigation.BottomNavigationView
>>>>>>> ca930b258180c4aa605bb11421216122730cb052

class HomeFragment : Fragment() {

  lateinit var binding: FragmentHomeBinding
  lateinit var viewModel: HomeViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {

    viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
    binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
    binding.viewModel = viewModel
    binding.lifecycleOwner = this

    binding.Explaining.setOnClickListener {
      val intent = Intent(requireContext(), Explaining::class.java)
      startActivity(intent)
    }

    binding.Prevention.setOnClickListener {
      val intent = Intent(requireContext(), Prevention::class.java)
      startActivity(intent)
    }

    return binding.root
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

<<<<<<< HEAD
    //observer()
  }

=======
    setSpanHome()
    observer()
  }

  private fun setSpanHome() {
    val span = SpannableString(binding.textCheckHealth.text).withClickableSpan(
      context = requireContext(),
      startChar = 19,
      endChar = 25
    ) {
      val bottomNavigationView =
        activity?.findViewById<BottomNavigationView>(R.id.bottom_navigation)
      bottomNavigationView?.selectedItemId = R.id.questionnaire
    }

    binding.textCheckHealth.movementMethod = LinkMovementMethod.getInstance()
    binding.textCheckHealth.setText(span, TextView.BufferType.SPANNABLE)
    binding.textCheckHealth.isClickable = true
  }

  private fun observer() {
    viewModel.response.observe(viewLifecycleOwner) {
      if (it.isNotEmpty()) {
        val homeAdapter = HomeAdapter(it) { model ->
          val intent = Intent(requireContext(), DetailActivity::class.java)
          intent.putExtra("intent_key", model)
          startActivity(intent)
        }
>>>>>>> ca930b258180c4aa605bb11421216122730cb052

//  private fun observer() {
//    viewModel.response.observe(viewLifecycleOwner) {
//      if (it.isNotEmpty()) {
//        val homeAdapter = HomeAdapter(it)
//
//        binding.recyclerViewHome.apply {
//          layoutManager = LinearLayoutManager(requireContext())
//          adapter = homeAdapter
//        }
//      }
//    }
//  }
}