package com.diagnosiscovid.features.main.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.diagnosiscovid.BR
import com.diagnosiscovid.data.repository.home.model.HomeIntroModel
import com.diagnosiscovid.databinding.ItemIntroHomeBinding

class HomeAdapter(
  private val introList: List<HomeIntroModel>,
  private val onClickListener: (HomeIntroModel) -> Unit
) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val inflater = LayoutInflater.from(parent.context)
    val viewDataBinding = ItemIntroHomeBinding.inflate(inflater, parent, false)
    return ViewHolder(viewDataBinding)
  }

  override fun getItemCount(): Int {
    return introList.size
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bind(introList[position], onClickListener)
  }

  class ViewHolder(private val viewDataBinding: ItemIntroHomeBinding) :
    RecyclerView.ViewHolder(viewDataBinding.root) {
    fun bind(model: HomeIntroModel, onClickListener: (HomeIntroModel) -> Unit) {
      viewDataBinding.setVariable(BR.model, model)
      viewDataBinding.executePendingBindings()

      Glide.with(viewDataBinding.root)
        .load(model.image)
        .into(viewDataBinding.imageHomeIntro)

      viewDataBinding.root.setOnClickListener { onClickListener.invoke(model) }
    }
  }

}