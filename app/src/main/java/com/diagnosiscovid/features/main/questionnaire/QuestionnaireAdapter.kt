package com.diagnosiscovid.features.main.questionnaire

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.diagnosiscovid.BR
import com.diagnosiscovid.R
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionModel
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionnairePayload
import com.diagnosiscovid.databinding.ItemQuestionBinding

class QuestionnaireAdapter(
  private val questions: List<QuestionModel>
) : RecyclerView.Adapter<QuestionnaireAdapter.ViewHolder>() {

  var payload = QuestionnairePayload()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val inflater = LayoutInflater.from(parent.context)
    val viewDataBinding = ItemQuestionBinding.inflate(inflater, parent, false)
    return ViewHolder(viewDataBinding)
  }

  override fun getItemCount(): Int {
    return questions.size
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bind(questions[position])
  }

  inner class ViewHolder(private val binding: ItemQuestionBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(model: QuestionModel) {
      binding.setVariable(BR.model, model)
      binding.executePendingBindings()

      binding.radioGroup.setOnCheckedChangeListener { _, checkedId ->
        when (checkedId) {
          R.id.radio_yes -> {
            model.isSymptomsTrue = true

            when (adapterPosition) {
              0 -> {
                payload = payload.copy(kode_gejala1 = model.idGejala)
              }
              1 -> {
                payload = payload.copy(kode_gejala2 = model.idGejala)
              }
              2 -> {
                payload = payload.copy(kode_gejala3 = model.idGejala)
              }
              3 -> {
                payload = payload.copy(kode_gejala4 = model.idGejala)
              }
              4 -> {
                payload = payload.copy(kode_gejala5 = model.idGejala)
              }
              5 -> {
                payload = payload.copy(kode_gejala6 = model.idGejala)
              }
              6 -> {
                payload = payload.copy(kode_gejala7 = model.idGejala)
              }
              7 -> {
                payload = payload.copy(kode_gejala8 = model.idGejala)
              }
              8 -> {
                payload = payload.copy(kode_gejala9 = model.idGejala)
              }
              9 -> {
                payload = payload.copy(kode_gejala10 = model.idGejala)
              }
              10 -> {
                payload = payload.copy(kode_gejala11 = model.idGejala)
              }
              11 -> {
                payload = payload.copy(kode_gejala12 = model.idGejala)
              }
            }
          }
          R.id.radio_no -> {
            model.isSymptomsTrue = false

            when (adapterPosition) {
              0 -> {
                payload = payload.copy(kode_gejala1 = model.idGejalaNoSymptoms)
              }
              1 -> {
                payload = payload.copy(kode_gejala2 = model.idGejalaNoSymptoms)
              }
              2 -> {
                payload = payload.copy(kode_gejala3 = model.idGejalaNoSymptoms)
              }
              3 -> {
                payload = payload.copy(kode_gejala4 = model.idGejalaNoSymptoms)
              }
              4 -> {
                payload = payload.copy(kode_gejala5 = model.idGejalaNoSymptoms)
              }
              5 -> {
                payload = payload.copy(kode_gejala6 = model.idGejalaNoSymptoms)
              }
              6 -> {
                payload = payload.copy(kode_gejala7 = model.idGejalaNoSymptoms)
              }
              7 -> {
                payload = payload.copy(kode_gejala8 = model.idGejalaNoSymptoms)
              }
              8 -> {
                payload = payload.copy(kode_gejala9 = model.idGejalaNoSymptoms)
              }
              9 -> {
                payload = payload.copy(kode_gejala10 = model.idGejalaNoSymptoms)
              }
              10 -> {
                payload = payload.copy(kode_gejala11 = model.idGejalaNoSymptoms)
              }
              11 -> {
                payload = payload.copy(kode_gejala12 = model.idGejalaNoSymptoms)
              }
            }
          }
        }
      }
    }
  }
}