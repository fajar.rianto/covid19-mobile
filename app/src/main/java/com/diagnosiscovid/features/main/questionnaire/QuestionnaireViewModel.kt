package com.diagnosiscovid.features.main.questionnaire

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.diagnosiscovid.data.repository.hasildiagnosis.HasilDiagnosisRepository
import com.diagnosiscovid.data.repository.hasildiagnosis.HasilDiagnosisResponse
import com.diagnosiscovid.data.repository.questionnaire.QuestionnaireRepository
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionModel
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionnairePayload
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionnaireResponse
import com.diagnosiscovid.network.NetworkBuilder
import com.diagnosiscovid.utils.PrefsUtils
import kotlinx.coroutines.launch

class QuestionnaireViewModel : ViewModel() {

  private val apiService = NetworkBuilder.getService()
  private val repo = QuestionnaireRepository(apiService)
  private val repoHasilDiagnose = HasilDiagnosisRepository(apiService)

  val questionsResponse = MutableLiveData<List<QuestionModel>>()
  val gejalaListResponse = MutableLiveData<List<QuestionnaireResponse>>()
  val isSuccessSubmitted = MutableLiveData<Boolean>()
  val diagnoseResults = MutableLiveData<List<HasilDiagnosisResponse>>()

  init {
    getQuestions()
    getGejalaList()
  }

  private fun getQuestions() {
    viewModelScope.launch {
      questionsResponse.value = repo.getQuestions()
    }
  }

  private fun getGejalaList() {
    viewModelScope.launch {
      gejalaListResponse.value = repo.getGejalaList()
    }
  }

  fun submitQuestionnaire(payload: QuestionnairePayload) {
    viewModelScope.launch {
      isSuccessSubmitted.value = repo.submitQuestionnaire(payload)?.message?.contains(
        "berhasil",
        ignoreCase = true
      )
    }
  }

  fun getDiagnoseResults() {
    viewModelScope.launch {
      diagnoseResults.value = repoHasilDiagnose.getDiagnoseResults().filter {
        it.email == PrefsUtils.getUser().email
      }
    }
  }

}