package com.diagnosiscovid.features.main.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.diagnosiscovid.data.repository.profile.UpdateProfileRequest
import com.diagnosiscovid.data.repository.user.UserRepository
import com.diagnosiscovid.data.repository.user.model.UserResponseItem
import com.diagnosiscovid.network.NetworkBuilder
import com.diagnosiscovid.utils.PrefsUtils
import kotlinx.coroutines.launch

class ProfileViewModel : ViewModel() {
  private var user = MutableLiveData(PrefsUtils.getUser())
  val getUser: LiveData<UserResponseItem>
    get() = user

  var name = MutableLiveData(user.value?.name)
  var gender = MutableLiveData(user.value?.gender)
  var address = MutableLiveData(user.value?.address)
  var username = MutableLiveData(user.value?.username)

  val isSuccess = MutableLiveData<Boolean>(null)

  fun saveUser() {
    val apiService = NetworkBuilder.getService()
    val repo = UserRepository(apiService)

    viewModelScope.launch {
      val response = repo.updateUser(
        user.value?.id,
        UpdateProfileRequest(
          name = name.value.toString(),
          jenis_kelamin = gender.value.toString(),
          alamat = address.value.toString(),
          username = username.value.toString()
        )
      )

      val userResponseItem = repo.getDetailUser(user.value?.id)
      PrefsUtils.saveUser(userResponseItem)

      isSuccess.value = response.message?.contains("berhasil", ignoreCase = true)
    }
  }

}