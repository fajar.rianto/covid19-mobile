package com.diagnosiscovid.features.signup

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.diagnosiscovid.R
import com.diagnosiscovid.databinding.ActivitySignUpBinding
import com.diagnosiscovid.features.signin.SignInActivity
import com.diagnosiscovid.utils.withClickableSpan

class SignUpActivity : AppCompatActivity() {

  private lateinit var binding: ActivitySignUpBinding
  private lateinit var viewModel: SignUpViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    viewModel = ViewModelProvider(this).get(SignUpViewModel::class.java)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
    binding.viewModel = viewModel
    binding.lifecycleOwner = this

    setSpanLogin()
    listener()
    observer()
  }

  private fun observer() {
    with(viewModel) {
      fullName.observe(this@SignUpActivity) {
        setEnabledButtonSignUp()
      }
      email.observe(this@SignUpActivity) {
        setEnabledButtonSignUp()
      }
      phoneNumber.observe(this@SignUpActivity) {
        setEnabledButtonSignUp()
      }
      password.observe(this@SignUpActivity) {
        setEnabledButtonSignUp()
      }
      confirmPassword.observe(this@SignUpActivity) {
        setEnabledButtonSignUp()
      }
      response.observe(this@SignUpActivity) {
        if (it != null) {
          when {
            it.success?.token?.isNotEmpty() == true -> {
              Toast.makeText(
                this@SignUpActivity,
                "Berhasil Registrasi. Silahkan Lakukan Login",
                Toast.LENGTH_LONG
              ).show()
              showLoginActivity()
            }
            it.success?.token.isNullOrEmpty() -> {
              enabledAllFields()
              binding.buttonSignUp.text = "Sign Up"
              binding.progressBar.visibility = View.GONE
            }
            it.success == null -> {
              Toast.makeText(
                this@SignUpActivity,
                "Terjadi Kesalahan, silahkan masukkan data dengan benar",
                Toast.LENGTH_SHORT
              ).show()
            }
          }
        }
      }
    }
  }

  private fun setEnabledButtonSignUp() {
    binding.buttonSignUp.isEnabled = viewModel.isValidToSignup()
  }

  private fun listener() {
    binding.buttonSignUp.setOnClickListener {
      disabledAllFields()
      binding.buttonSignUp.text = ""
      binding.progressBar.visibility = View.VISIBLE
      viewModel.signUp()
    }

    val items = listOf("Laki-Laki", "Perempuan")
    val adapter = ArrayAdapter(this, R.layout.gender_item, items)
    binding.autoCompleteGender.setAdapter(adapter)
    binding.autoCompleteGender.keyListener = null
  }

  private fun enabledAllFields() {
    binding.layoutInputFullName.isEnabled = true
    binding.layoutInputUsername.isEnabled = true
    binding.layoutInputEmail.isEnabled = true
    binding.layoutInputPhoneNumber.isEnabled = true
    binding.layoutInputGender.isEnabled = true
    binding.layoutInputAddress.isEnabled = true
    binding.layoutInputPassword.isEnabled = true
    binding.layoutInputConfirmPassword.isEnabled = true
  }

  private fun disabledAllFields() {
    binding.layoutInputFullName.isEnabled = false
    binding.layoutInputUsername.isEnabled = false
    binding.layoutInputEmail.isEnabled = false
    binding.layoutInputPhoneNumber.isEnabled = false
    binding.layoutInputGender.isEnabled = false
    binding.layoutInputAddress.isEnabled = false
    binding.layoutInputPassword.isEnabled = false
    binding.layoutInputConfirmPassword.isEnabled = false
  }

  private fun setSpanLogin() {
    val span = SpannableString(binding.textSignIn.text).withClickableSpan(
      context = this,
      startChar = 24,
      endChar = 30
    ) { showLoginActivity() }

    binding.textSignIn.movementMethod = LinkMovementMethod.getInstance()
    binding.textSignIn.setText(span, TextView.BufferType.SPANNABLE)
    binding.textSignIn.isClickable = true
  }

  private fun showLoginActivity() {
    val intent = Intent(this, SignInActivity::class.java)
    startActivity(intent)
    finish()
  }

}