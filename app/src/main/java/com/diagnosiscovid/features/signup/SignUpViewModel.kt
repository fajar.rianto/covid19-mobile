package com.diagnosiscovid.features.signup

import androidx.core.text.isDigitsOnly
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.diagnosiscovid.data.repository.signup.SignUpRepository
import com.diagnosiscovid.data.repository.signup.model.SignUpRequestModel
import com.diagnosiscovid.data.repository.signup.model.SignUpResponse
import com.diagnosiscovid.network.NetworkBuilder
import com.diagnosiscovid.utils.isValidEmail
import kotlinx.coroutines.launch

class SignUpViewModel : ViewModel() {

  private val apiService = NetworkBuilder.getService()
  private val repository = SignUpRepository(apiService)

  val fullName = MutableLiveData<String>(null)
  val username = MutableLiveData<String>(null)
  val email = MutableLiveData<String>(null)
  val phoneNumber = MutableLiveData<String>(null)
  val gender = MutableLiveData<String>(null)
  val address = MutableLiveData<String>(null)
  val password = MutableLiveData<String>(null)
  val confirmPassword = MutableLiveData<String>(null)

  val response = MutableLiveData<SignUpResponse>()

  fun isValidToSignup(): Boolean {
    val isValidPassword = password.value == confirmPassword.value &&
      password.value?.isNotBlank() == true &&
      confirmPassword.value?.isNotBlank() == true &&
      password.value?.length ?: 0 >= 8 &&
      confirmPassword.value?.length ?: 0 >= 8

    val isValidPhoneNumber = phoneNumber.value?.isNotBlank() == true &&
      phoneNumber.value?.isDigitsOnly() == true

    return fullName.value?.isNotBlank() == true &&
      username.value?.isNotBlank() == true &&
      email.value?.isValidEmail() == true &&
      gender.value?.isNotBlank() == true &&
      address.value?.isNotBlank() == true &&
      isValidPhoneNumber && isValidPassword
  }

  fun signUp() {
    viewModelScope.launch {
      response.value = repository.signUp(
        SignUpRequestModel(
          fullName.value.toString(),
          username.value.toString(),
          email.value.toString(),
          phoneNumber.value.toString(),
          password.value.toString(),
          confirmPassword.value.toString(),
          gender = gender.value.toString(),
          address = address.value.toString()
        )
      )
    }
  }

}