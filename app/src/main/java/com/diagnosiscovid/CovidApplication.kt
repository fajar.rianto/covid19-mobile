package com.diagnosiscovid

import android.app.Application
import com.diagnosiscovid.utils.PrefsUtils

class CovidApplication : Application() {

  override fun onCreate() {
    super.onCreate()
    PrefsUtils.initPref(this)
  }

}