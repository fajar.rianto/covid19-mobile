package com.diagnosiscovid.data.repository.hasildiagnosis

import com.diagnosiscovid.network.ApiService

class HasilDiagnosisRepository(private val apiService: ApiService) {

  suspend fun getDiagnoseResults(): List<HasilDiagnosisResponse> {
    return try {
      apiService.diagnoseResults()
    } catch (exception: Exception) {
      emptyList()
    }
  }

}