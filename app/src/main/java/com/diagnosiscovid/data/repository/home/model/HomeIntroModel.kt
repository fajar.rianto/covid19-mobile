package com.diagnosiscovid.data.repository.home.model

import android.os.Parcelable
import androidx.annotation.DrawableRes
import kotlinx.parcelize.Parcelize

@Parcelize
data class HomeIntroModel(
  val id: String,
  val name: String,
  @DrawableRes val image: Int,
  val detail: Detail
) : Parcelable

@Parcelize
data class Detail(
  @DrawableRes val imageHeader: Int,
  @DrawableRes val imageDesc: Int
) : Parcelable