package com.diagnosiscovid.data.repository.signup.model

import com.google.gson.annotations.SerializedName

data class SignUpRequestModel(
  @SerializedName("name")
  var fullName: String? = null,
  @SerializedName("username")
  var username: String? = null,
  @SerializedName("email")
  var email: String? = null,
  @SerializedName("no_hp")
  var phoneNumber: String? = null,
  @SerializedName("password")
  var password: String? = null,
  @SerializedName("c_password")
  var confirmPassword: String? = null,
  @SerializedName("role")
  var role: String = "Pengunjung",
  @SerializedName("jenis_kelamin")
  var gender: String,
  @SerializedName("alamat")
  var address: String
)