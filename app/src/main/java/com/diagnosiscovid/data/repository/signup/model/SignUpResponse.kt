package com.diagnosiscovid.data.repository.signup.model

data class SignUpResponse(
  val success: SignUpResponseModel?
)

data class SignUpResponseModel(
  val token: String?,
  val name: String?
)