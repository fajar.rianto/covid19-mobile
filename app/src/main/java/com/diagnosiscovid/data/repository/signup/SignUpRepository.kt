package com.diagnosiscovid.data.repository.signup

import com.diagnosiscovid.data.repository.signup.model.SignUpRequestModel
import com.diagnosiscovid.data.repository.signup.model.SignUpResponse
import com.diagnosiscovid.network.ApiService

class SignUpRepository(private val apiService: ApiService) {

  suspend fun signUp(requestModel: SignUpRequestModel): SignUpResponse {
    val signUpResponse: SignUpResponse = try {
      apiService.signUp(requestModel)
    } catch (exception: Exception) {
      println(exception.toString())
      SignUpResponse(null)
    }

    return signUpResponse
  }

}