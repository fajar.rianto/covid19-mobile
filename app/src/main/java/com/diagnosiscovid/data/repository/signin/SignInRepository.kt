package com.diagnosiscovid.data.repository.signin

import com.diagnosiscovid.data.repository.signin.model.SignInResponse
import com.diagnosiscovid.network.ApiService

class SignInRepository(private val apiService: ApiService) {

  suspend fun signIn(email: String, password: String): SignInResponse {
    val signInResponse: SignInResponse = try {
      apiService.signIn(email, password)
    } catch (exception: Exception) {
      SignInResponse(null, exception.message)
    }

    return signInResponse
  }

}