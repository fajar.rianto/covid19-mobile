package com.diagnosiscovid.data.repository.signin.model

data class SignInResponse(
  val success: SignInResponseItem?,
  val error: String?
)

data class SignInResponseItem(
  val token: String?
)