package com.diagnosiscovid.data.repository.questionnaire.model

import com.diagnosiscovid.utils.PrefsUtils

data class QuestionnairePayload(
  val email: String = PrefsUtils.getUser().email.toString(),
  val kode_gejala1: String? = null,
  val kode_gejala2: String? = null,
  val kode_gejala3: String? = null,
  val kode_gejala4: String? = null,
  val kode_gejala5: String? = null,
  val kode_gejala6: String? = null,
  val kode_gejala7: String? = null,
  val kode_gejala8: String? = null,
  val kode_gejala9: String? = null,
  val kode_gejala10: String? = null,
  val kode_gejala11: String? = null,
  val kode_gejala12: String? = null
)