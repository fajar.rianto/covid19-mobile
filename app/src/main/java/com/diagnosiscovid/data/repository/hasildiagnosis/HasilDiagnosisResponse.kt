package com.diagnosiscovid.data.repository.hasildiagnosis

data class HasilDiagnosisResponse(
  val id: Int,
  val email: String,
  val kode_gejala1: String,
  val kode_gejala2: String,
  val kode_gejala3: String,
  val kode_gejala4: String,
  val kode_gejala5: String,
  val kode_gejala6: String,
  val kode_gejala7: String,
  val kode_gejala8: String,
  val kode_gejala9: String,
  val kode_gejala10: String,
  val kode_gejala11: String,
  val kode_gejala12: String,
  val tanggal: String,
  val hasil: String?,
)
