package com.diagnosiscovid.data.repository.questionnaire.model

import com.google.gson.annotations.SerializedName

data class QuestionnaireResponse(
  val id: Int,
  @SerializedName("kode_gejala")
  val kodeGejala: String,
  @SerializedName("nama_gejala")
  val namaGejala: String
) {
  constructor() : this(0, "", "")
}