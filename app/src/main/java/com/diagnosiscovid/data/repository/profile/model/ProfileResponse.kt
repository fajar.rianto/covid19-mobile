package com.diagnosiscovid.data.repository.profile.model

data class ProfileResponse(
    val id: String
)
