package com.diagnosiscovid.data.repository.questionnaire.model

data class QuestionModel(
  val id: String,
  val idGejala: String,
  val question: String,
  var isSymptomsTrue: Boolean? = null,
  var idGejalaNoSymptoms: String
) {
  override fun toString(): String {
    return "$id $idGejala $question $isSymptomsTrue"
  }
}
