package com.diagnosiscovid.data.repository.profile

import com.diagnosiscovid.utils.PrefsUtils

data class UpdateProfileRequest(
  val name: String,
  val jenis_kelamin: String,
  val alamat: String,
  val username: String,
  val email: String = PrefsUtils.getUser().email.toString(),
  val no_hp: String = PrefsUtils.getUser().noHp.toString(),
  val role: String = PrefsUtils.getUser().role.toString()
)