package com.diagnosiscovid.data.repository.profile.model

data class UpdateProfileResponse(
  val message: String?
)