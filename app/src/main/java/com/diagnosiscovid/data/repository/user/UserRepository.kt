package com.diagnosiscovid.data.repository.user

import com.diagnosiscovid.data.repository.profile.UpdateProfileRequest
import com.diagnosiscovid.data.repository.profile.model.UpdateProfileResponse
import com.diagnosiscovid.data.repository.user.model.UserResponseItem
import com.diagnosiscovid.network.ApiService

class UserRepository(private val apiService: ApiService) {

  suspend fun getUsers(email: String): UserResponseItem {
    val userItem: UserResponseItem = try {
      val response = apiService.getAllUsers().filter {
        it.email == email
      }
      response[0]
    } catch (exception: Exception) {
      UserResponseItem(null, null, null, null, null, null, null, null, null, null, null)
    }

    return userItem
  }

  suspend fun updateUser(id: Int?, request: UpdateProfileRequest): UpdateProfileResponse {
    return try {
      apiService.updateProfile(id, request)
    } catch (exception: Exception) {
      UpdateProfileResponse(null)
    }
  }

  suspend fun getDetailUser(id: Int?): UserResponseItem {
    return try {
      apiService.getDetailUser(id)
    } catch (exception: Exception) {
      UserResponseItem(null, null, null, null, null, null, null, null, null, null, null)
    }
  }

}