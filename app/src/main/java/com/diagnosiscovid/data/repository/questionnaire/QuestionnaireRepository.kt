package com.diagnosiscovid.data.repository.questionnaire

import com.diagnosiscovid.data.repository.questionnaire.model.QuestionModel
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionnairePayload
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionnaireResponse
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionnaireSubmitResponse
import com.diagnosiscovid.network.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class QuestionnaireRepository(private val apiService: ApiService) {

  suspend fun getQuestions(): List<QuestionModel> = withContext(Dispatchers.IO) {
    listOf(
      QuestionModel(
        id = "1",
        idGejala = "G1",
        question = "Apakah Anda demam?",
        idGejalaNoSymptoms = "G13"
      ),
      QuestionModel(
        id = "2",
        idGejala = "G2",
        question = "Apakah Anda pusing?",
        idGejalaNoSymptoms = "G14"
      ),
      QuestionModel(
        id = "3",
        idGejala = "G3",
        question = "Apakah Anda bersin-bersin?",
        idGejalaNoSymptoms = "G15"
      ),
      QuestionModel(
        id = "4",
        idGejala = "G4",
        question = "Apakah Anda batuk?",
        idGejalaNoSymptoms = "G16"
      ),
      QuestionModel(
        id = "5",
        idGejala = "G5",
        question = "Apakah Anda sakit tenggorokan?",
        idGejalaNoSymptoms = "G17"
      ),
      QuestionModel(
        id = "6",
        idGejala = "G6",
        question = "Apakah Anda kelelahan?",
        idGejalaNoSymptoms = "G18"
      ),
      QuestionModel(
        id = "7",
        idGejala = "G7",
        question = "Apakah Anda terdapat nyeri di dada?",
        idGejalaNoSymptoms = "G19"
      ),
      QuestionModel(
        id = "8",
        idGejala = "G8",
        question = "Apakah Anda terkena diare?",
        idGejalaNoSymptoms = "G20"
      ),
      QuestionModel(
        id = "9",
        idGejala = "G9",
        question = "Apakah penciuman Anda hilang?",
        idGejalaNoSymptoms = "G21"
      ),
      QuestionModel(
        id = "10",
        idGejala = "G10",
        question = "Apakah indera perasa Anda hilang ?",
        idGejalaNoSymptoms = "G22"
      ),
      QuestionModel(
        id = "11",
        idGejala = "G11",
        question = "Apakah Anda kehilangan napsu makan?",
        idGejalaNoSymptoms = "G23"
      ),
      QuestionModel(
        id = "12",
        idGejala = "G12",
        question = "Apakah Anda merasa nyeri pada sendi?",
        idGejalaNoSymptoms = "G24"
      )
    )
  }

  suspend fun getGejalaList(): List<QuestionnaireResponse> {
    val response: List<QuestionnaireResponse> = try {
      apiService.getGejalaList()
    } catch (exception: Exception) {
      emptyList()
    }

    return response
  }

  suspend fun submitQuestionnaire(payload: QuestionnairePayload): QuestionnaireSubmitResponse? {
    return try {
      apiService.submitQuestionnaire(payload)
    } catch (exception: Exception) {
      null
    }
  }

}