package com.diagnosiscovid.data.repository.questionnaire.model

data class QuestionnaireSubmitResponse(
  val message: String?
)
