package com.diagnosiscovid.data.repository.user.model

import com.google.gson.annotations.SerializedName

data class UserResponseItem(
  @SerializedName("created_at")
    val createdAt: String?,
  @SerializedName("email")
    val email: String?,
  @SerializedName("email_verified_at")
    val emailVerifiedAt: Any?,
  @SerializedName("id")
  val id: Int?,
  @SerializedName("name")
  val name: String?,
  @SerializedName("no_hp")
  val noHp: String?,
  @SerializedName("role")
  val role: String?,
  @SerializedName("updated_at")
  val updatedAt: String?,
  @SerializedName("username")
  val username: String?,
  @SerializedName("jenis_kelamin")
  val gender: String?,
  @SerializedName("alamat")
  val address: String?
)