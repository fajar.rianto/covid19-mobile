package com.diagnosiscovid.data.repository.home

import com.diagnosiscovid.R
import com.diagnosiscovid.data.repository.home.model.Detail
import com.diagnosiscovid.data.repository.home.model.HomeIntroModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class HomeRepository {

  suspend fun getIntroList(): List<HomeIntroModel> {
    return getIntroListLocal()
  }

  private suspend fun getIntroListLocal() = withContext(Dispatchers.IO) {
    listOf(
      HomeIntroModel(
        "1",
        "Apa itu Covid-19?",
        R.drawable.ic_corona_1,
        Detail(
          R.drawable.detail_intro_header_1,
          R.drawable.detail_intro_desc_1,
        )
      ),
      HomeIntroModel(
        "2",
        "Penyebab dan Gejala Covid-19",
        R.drawable.ic_corona_2,
        Detail(
          R.drawable.detail_intro_header_2,
          R.drawable.detail_intro_desc_2,
        )
      ),
      HomeIntroModel(
        "3",
        "Cara Pencegahan Covid-19",
        R.drawable.ic_corona_3,
        Detail(
          R.drawable.detail_intro_header_3,
          R.drawable.detail_intro_desc_3,
        )
      )
    )
  }

}