package com.diagnosiscovid.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.diagnosiscovid.data.repository.user.model.UserResponseItem
import com.google.gson.Gson

object PrefsUtils {

    private const val NAME_PREF = "covid19"
    private const val TOKEN_KEY = "token"
    private const val USER_KEY = "user"
  private const val SUBMIT_QUESTIONNAIRE = "submit_questionnaire"

    private lateinit var sharedPreferences: SharedPreferences

    fun initPref(context: Context) {
        sharedPreferences = context.getSharedPreferences(NAME_PREF, Context.MODE_PRIVATE)
    }

    fun saveToken(token: String) {
        sharedPreferences.edit {
            putString(TOKEN_KEY, token)
        }
    }

    fun saveUser(payload: UserResponseItem) {
      sharedPreferences.edit {
        putString(USER_KEY, Gson().toJson(payload).toString())
      }
    }

  fun getUser() = Gson().fromJson(
    sharedPreferences.getString(USER_KEY, ""),
    UserResponseItem::class.java
  )

  fun saveUserSubmittedQuestionnaire() {
    sharedPreferences.edit {
      putBoolean(SUBMIT_QUESTIONNAIRE, true)
    }
  }

  fun hasUserHasSubmitQuestionnaire() = sharedPreferences.getBoolean(SUBMIT_QUESTIONNAIRE, false)

  fun clearSession() {
    sharedPreferences.edit().remove(TOKEN_KEY).apply()
    sharedPreferences.edit().remove(USER_KEY).apply()
    sharedPreferences.edit().remove(SUBMIT_QUESTIONNAIRE).apply()
    sharedPreferences.edit().clear().apply()
  }

  fun isUserSignedIn(): Boolean? = sharedPreferences.getString(TOKEN_KEY, "")?.isNotEmpty()

}