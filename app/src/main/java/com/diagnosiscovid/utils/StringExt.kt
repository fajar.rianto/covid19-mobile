package com.diagnosiscovid.utils

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.core.content.ContextCompat
import com.diagnosiscovid.R

fun SpannableString.withClickableSpan(
  context: Context,
  startChar: Int,
  endChar: Int,
  onClickListener: () -> Unit
): SpannableString {
  val clickableSpan = object : ClickableSpan() {
    override fun onClick(p0: View) {
      onClickListener.invoke()
    }

    override fun updateDrawState(ds: TextPaint) {
      ds.isUnderlineText = false
    }
  }

  this.setSpan(
    clickableSpan,
    startChar, endChar,
    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
  )

  this.setSpan(
    ForegroundColorSpan(ContextCompat.getColor(context, R.color.purple_755FE2)),
    startChar, endChar,
    Spanned.SPAN_INCLUSIVE_EXCLUSIVE
  )

  return this
}