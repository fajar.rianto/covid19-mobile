package com.diagnosiscovid.network

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkBuilder {

  private fun loggingInterceptor(): OkHttpClient {
    val logging = HttpLoggingInterceptor { message ->
      Log.d("COV-19", message)
    }.setLevel(HttpLoggingInterceptor.Level.BODY)

    return OkHttpClient.Builder()
      .addInterceptor(logging)
      .build()
  }

  private val retrofitInstance = Retrofit.Builder()
    .baseUrl("https://diagnosiscovid.online/api/")
    .client(loggingInterceptor())
    .addConverterFactory(GsonConverterFactory.create())
    .build()

  fun getService(): ApiService = retrofitInstance.create(ApiService::class.java)

}