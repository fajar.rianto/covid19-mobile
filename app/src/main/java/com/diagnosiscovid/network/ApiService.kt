package com.diagnosiscovid.network

import com.diagnosiscovid.data.repository.hasildiagnosis.HasilDiagnosisResponse
import com.diagnosiscovid.data.repository.profile.UpdateProfileRequest
import com.diagnosiscovid.data.repository.profile.model.ProfileResponse
import com.diagnosiscovid.data.repository.profile.model.UpdateProfileResponse
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionnairePayload
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionnaireResponse
import com.diagnosiscovid.data.repository.questionnaire.model.QuestionnaireSubmitResponse
import com.diagnosiscovid.data.repository.signin.model.SignInResponse
import com.diagnosiscovid.data.repository.signup.model.SignUpRequestModel
import com.diagnosiscovid.data.repository.signup.model.SignUpResponse
import com.diagnosiscovid.data.repository.user.model.UserResponse
import com.diagnosiscovid.data.repository.user.model.UserResponseItem
import retrofit2.http.*

interface ApiService {

  @POST("register")
  suspend fun signUp(
    @Body signUpRequest: SignUpRequestModel
  ): SignUpResponse

  @POST("login")
  suspend fun signIn(
    @Query("email") email: String,
    @Query("password") password: String
  ): SignInResponse

  @GET("read_gejala")
  suspend fun getGejalaList(): List<QuestionnaireResponse>

  @GET("read_user")
  suspend fun getAllUsers(): UserResponse

  @GET("get_user/{id}")
  suspend fun getDetailUser(
    @Path("id") id: Int?
  ): UserResponseItem

  @GET("read_profile")
  suspend fun getProfile(): ProfileResponse

  @PATCH("update_user/{id}")
  suspend fun updateProfile(
    @Path("id") id: Int?,
    @Body request: UpdateProfileRequest
  ): UpdateProfileResponse

  @POST("data_rule")
  suspend fun submitQuestionnaire(
    @Body payload: QuestionnairePayload
  ): QuestionnaireSubmitResponse

  @GET("hasil_diagnosa")
  suspend fun diagnoseResults(): List<HasilDiagnosisResponse>

}
